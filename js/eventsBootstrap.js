$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        container: 'body'
      });
      $(".carousel").carousel({
        interval: 3000
      });

      //Eventos con el componente modal
      $("#miModal").on("shown.bs.modal", function(e){
        console.log("Terminando de abrir el modal");
      });
      $("#miModal").on("show.bs.modal", function(e){
        console.log("Empezando a abrir el modal");
        var identificador = $("#auxiliar").val();
        $("#"+identificador).addClass("btn-info");
        $("#"+identificador).attr("disabled", true);
      });
      $("#miModal").on("hide.bs.modal", function(e){
        console.log("Emepzando a cerrar el modal");
      });
      $("#miModal").on("hidden.bs.modal", function(e){
        console.log("Cerrando por completo el modal");
        var identificador = $("#auxiliar").val();
        $("#"+identificador).removeClass("btn-info");
        $("#"+identificador).attr("disabled", false);
        $("#"+identificador).addClass("btn-dark");
      });
      //Evento para desactivar el boton hasta que se cierre el modal
      $(".btn-comprar").click(function(){
        //alert("Gracias por comprar");
        var identificador = $(this).attr('id');
        //$("#"+identificador).attr("disabled", true);
        $("#"+identificador).removeClass("btn-dark");
        $("#auxiliar").val(identificador);
      });
});